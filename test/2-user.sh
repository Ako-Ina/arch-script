#!/usr/bin/env bash

echo -e "\nINSTALLING AUR SOFTWARE\n"

echo "CLONING: Aura"
cd ~
git clone https://aur.archlinux.org/aura-bin.git
cd aura-bin
makepkg -si --noconfirm
sudo aura -Syu
sudo aura -A dxvk-bin heroic-games-launcher-bin mangohud-common-git goverlay-git --noconfirm
setup_dxvk install

echo Installing spotify
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | gpg --import -
git clone https://aur.archlinux.org/spotify.git
    #remove last command from PKGBUILD... this fixes the PKGBUILD file so it will build
sudo sed '/chmod -R go-w "${pkgdir}"/d' /home/$USER/spotify/PKGBUILD
cd spotify
makepkg -si --noconfirm
cd
sudo aura -A spotify-adblock --noconfirm

echo -e "\nDone!\n"
exit
